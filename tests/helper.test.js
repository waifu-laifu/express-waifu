const { checker } = require('../src/helper')

const arr = ['name', 'age', 'alias', 'birth', 'bwh']
const threeSize = ['bust', 'waist', 'hips']
const error = Error('error example')

const obj = {
  name: 'Dummy',
  age: 10,
  alias: ['DumDum'],
  num: [10, 20],
  birth: new Date(),
  bwh: {
    bust: 10,
    waist: 10,
    hips: 10
  },
  three: {
    bust: 10,
    waist: '10',
    hps: 10
  },
  size: {
    bust: 10,
    waist: 10
  },
  falsy: null
}

describe('Tool check', () => {
  it('Must return Error if not string', () => {
    expect(checker.isString(obj.name) instanceof Error).not.toEqual(error instanceof Error)
    expect(checker.isString(obj.age) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isString(obj.alias) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isString(obj.birth) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isString(obj.bwh) instanceof Error).toEqual(error instanceof Error)
  })

  it('Must return Error if not number', () => {
    expect(checker.isNumber(obj.name) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isNumber(obj.age) instanceof Error).not.toEqual(error instanceof Error)
    expect(checker.isNumber(obj.alias) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isNumber(obj.birth) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isNumber(obj.bwh) instanceof Error).toEqual(error instanceof Error)
  })

  it('Must return Error if not date', () => {
    expect(checker.isDate(obj.name) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isDate(obj.age) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isDate(obj.alias) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isDate(obj.birth) instanceof Error).not.toEqual(error instanceof Error)
    expect(checker.isDate(obj.bwh) instanceof Error).toEqual(error instanceof Error)
  })

  it('Must return Error if not array', () => {
    expect(checker.isArray(checker.isString, checker.isError, obj.name) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isArray(checker.isString, checker.isError, obj.age) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isArray(checker.isString, checker.isError, obj.alias) instanceof Error).not.toEqual(error instanceof Error)
    expect(checker.isArray(checker.isString, checker.isError, obj.birth) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isArray(checker.isString, checker.isError, obj.bwh) instanceof Error).toEqual(error instanceof Error)
  })

  it('Must return Error if not object', () => {
    expect(checker.isObject(threeSize, checker.isNumber, checker.isError, obj.name, false) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isObject(threeSize, checker.isNumber, checker.isError, obj.age, false) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isObject(threeSize, checker.isNumber, checker.isError, obj.alias, false) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isObject(threeSize, checker.isNumber, checker.isError, obj.birth, false) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isObject(threeSize, checker.isNumber, checker.isError, obj.bwh, false) instanceof Error).not.toEqual(error instanceof Error)
  })

  it('Must return Falsy if parameter is Falsy', () => {
    expect(checker.isString(obj.falsy)).toBeFalsy()
    expect(checker.isNumber(obj.falsy)).toBeFalsy()
    expect(checker.isDate(obj.falsy)).toBeFalsy()
    expect(checker.isObject(threeSize, checker.isNumber, checker.isError, obj.falsy, false)).toBeFalsy()
    expect(checker.isArray(checker.isString, checker.isError, obj.falsy)).toBeFalsy()
  })

  it('Array must contain same data type', () => {
    expect(checker.isArray(checker.isString, checker.isError, obj.num) instanceof Error).toEqual(error instanceof Error)
  })

  it('Object must contain same key no more no less if set true', () => {
    expect(checker.isObject(arr, checker.isNumber, checker.isError, obj, true) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isObject(threeSize, checker.isNumber, checker.isError, obj.size, true) instanceof Error).toEqual(error instanceof Error)
    expect(checker.isObject(threeSize, checker.isNumber, checker.isError, obj.three, true) instanceof Error).toEqual(error instanceof Error)
  })

  it('Object must contain same data type', () => {
    expect(checker.isObject(threeSize, checker.isString, checker.isError, obj.bwh, false) instanceof Error).toEqual(error instanceof Error)
  })
})
