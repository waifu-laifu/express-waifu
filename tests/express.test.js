const app = require('../src')
const supertest = require('supertest')
const request = supertest(app)

describe('Express Test', () => {
  test('Can access root endpoint', async done => {
    const res = await request.get('/')
    expect(res.status).toEqual(200)
    done()
  })
  test('Can NOT access method except GET, POST, SEARCH, PUT, PATCH, DELETE', async done => {
    const res = await request.options('/')
    expect(res.status).toEqual(405)
    done()
  })
  test('Must return 404 if endpoint not found', async done => {
    const res = await request.get('/muda')
    expect(res.status).toEqual(404)
    done()
  })
})
