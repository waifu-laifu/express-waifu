const { waifu: { waifuBuild, waifuHandle } } = require('../src/component')
const { checker } = require('../src/helper')
const app = require('../src')
const supertest = require('supertest')
const request = supertest(app)

var id

const waifuOne = {
  name: {
    fname: 'Project',
    lname: 'Bunny',
    mname: 19
  }
}

const waifuTwo = {
  fullname: 19
}

const waifuThree = {
  fullname: 'Project Bunny 19C',
  age: 14
}

const waifuFour = {
  fullname: 'Bronya Zaychik',
  age: 16
}

const builtOne = waifuBuild(checker, waifuOne)
const builtTwo = waifuBuild(checker, waifuTwo)
const builtThree = waifuBuild(checker, waifuThree)

const error = new Error()

describe('Waifu Test', () => {
  describe('Success Test', () => {
    describe('Model and Build', () => {
      test('Must not return error', () => {
        expect(builtThree instanceof Error).not.toEqual(error instanceof Error)
      })
    })

    describe('Route', () => {
      test('Can POST waifu', async done => {
        const res = await request.post('/waifu').send(waifuThree)
        expect(res.status).toEqual(201)
        expect(res.body.message instanceof Error).not.toEqual(error instanceof Error)
        done()
      })

      test('Can GET waifu', async done => {
        const res = await request.get('/waifu')
        expect(res.status).toEqual(200)
        expect(res.body.message instanceof Error).not.toEqual(error instanceof Error)
        id = res.body.data[0]._id
        done()
      })

      test('Can PATCH waifu', async done => {
        const res = await request.patch(`/waifu/${id}`).send({ fullname: 'Bronya Zaychik' })
        expect(res.status).toEqual(200)
        expect(res.body.message instanceof Error).not.toEqual(error instanceof Error)
        done()
      })

      test('Can GET waifu detail', async done => {
        const res = await request.get(`/waifu/${id}`)
        expect(res.status).toEqual(200)
        expect(res.body.message instanceof Error).not.toEqual(error instanceof Error)
        done()
      })

      test('Can PUT waifu', async done => {
        const res = await request.put(`/waifu/${id}`).send(waifuFour)
        expect(res.status).toEqual(201)
        expect(res.body.message instanceof Error).not.toEqual(error instanceof Error)
        done()
      })

      test('Can SEARCH waifu', async done => {
        const res = await request.search('/waifu').send(waifuFour)
        expect(res.status).toEqual(200)
        expect(res.body.message instanceof Error).not.toEqual(error instanceof Error)
        id = res.body.data[0]._id
        done()
      })

      test('Can DELETE waifu', async done => {
        const res = await request.delete(`/waifu/${id}`)
        expect(res.status).toEqual(200)
        expect(res.body.message instanceof Error).not.toEqual(error instanceof Error)
        done()
      })
    })
  })

  describe('Error Test', () => {
    describe('Model and Build', () => {
      test('Must return error if object or array value contain error', () => {
        expect(builtOne instanceof Error).toEqual(error instanceof Error)
      })

      test('Must return error if value contain error', () => {
        expect(builtTwo instanceof Error).toEqual(error instanceof Error)
      })
    })

    describe('Handler and Query', () => {
      test('Must throw error if query error', async done => {
        try {
          await waifuHandle.detailWaifu(id + 12)
        } catch (err) {
          expect(err instanceof Error).toEqual(error instanceof Error)
        }
        done()
      })
    })

    describe('Route', () => {
      test('Must return 500 if server error', async done => {
        const res = await request.get(`/waifu/${id + 12}`)
        expect(res.status).toEqual(500)
        done()
      })

      test('Can NOT access custom route', async done => {
        const res = await request.get('/waifu/for/laifu')
        expect(res.status).toEqual(404)
        done()
      })

      test('Can NOT POST waifu without body', async done => {
        const res = await request.post('/waifu').send()
        expect(res.status).toEqual(400)
        done()
      })

      test('Must return 404 if no waifu found on SEARCH or GET', async done => {
        const res = await request.search('/waifu').send({ fullname: 'Seele Vollerei' })
        expect(res.status).toEqual(404)
        done()
      })

      test('Must return 400 if cannot DELETE or POST', async done => {
        const res = await request.delete(`/waifu/${id.toLowerCase()}`)
        expect(res.status).toEqual(400)
        done()
      })

      test('Must return 400 if no waifu found on PUT or PATCH', async done => {
        const res = await request.put(`/waifu/${id.toLowerCase()}`).send({ fullname: 'Seele Vollerei' })
        expect(res.status).toEqual(400)
        done()
      })
    })
  })
})
