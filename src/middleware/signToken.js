const jwt = require('jsonwebtoken')
const { readFileSync } = require('fs')
const config = require('../../config')

const privateRSA = readFileSync('./config/privateRSA.pem', 'utf8')

module.exports = (data) => {
  let userData = {
    user_id: data.user_id,
    user_fullname: data.user_fullname,
    user_roles: data.user_roles
  }
  let token = jwt.sign(userData, { key: privateRSA, passphrase: config.cCreate.token }, config.jwt.options)
  userData.token = token

  return userData
}
