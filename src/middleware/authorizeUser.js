const jwt = require('jsonwebtoken')
const { readFileSync } = require('fs')
const config = require('../../config')

const publicRSA = readFileSync('./config/publicRSA.pem', 'utf8')

module.exports = (req, res, next) => {
  let token = req.body.token || req.query.token || req.headers['x-access-token'] || req.get('x-access-token') || req.get('authorization')

  if (token && token !== '') {
    try {
      let decoded = jwt.verify(token, publicRSA, config.jwt.options)
      req.access = decoded

      next()
    } catch (err) {
      console.log(err)
      err.status = 401

      throw err
    }
  } else {
    if (req.path === '/auth/login' || req.path === '/auth/register') {
      next()
    } else {
      res.status(401).json({
        status: 401,
        message: 'No Token Detected, Please Send Token'
      })
    }
  }
}
