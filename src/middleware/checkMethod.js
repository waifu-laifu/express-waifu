const method = ['GET', 'SEARCH', 'POST', 'PUT', 'PATCH', 'DELETE']

module.exports = (req, res, next) => {
  if (method.includes(req.method)) {
    next()
  } else {
    let err = Error(`You Cannot Use This Method On This Server (${req.method})`)
    err.status = 405
    err.name = 'MethodNotAllowed'

    throw err
  }
}
