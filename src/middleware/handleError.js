module.exports = (err, req, res, next) => {
  console.log(err)

  const errorObject = Object.assign({
    status: 500,
    name: 'InternalServerError',
    message: 'Something Bad Happen When Processing Your Request'
  }, err)

  res.status(err.status || 500).json(errorObject)
}
