module.exports = (req) => {
  const { path, method, body, query, params } = req

  return Object.freeze({
    path,
    method,
    body,
    query,
    params
  })
}
