const requireBody = ['SEARCH', 'POST', 'PUT', 'PATCH']

module.exports = (req, res, next) => {
  if (requireBody.includes(req.method)) {
    if (Object.keys(req.body).length === 0) {
      let err = Error('No Data Passed, Nothing To Do')
      err.status = 400
      err.name = 'BadRequest'

      throw err
    } else {
      next()
    }
  } else {
    next()
  }
}
