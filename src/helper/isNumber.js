module.exports = (data) => {
  if (!data) {
    return data
  } else if (typeof data === 'number' && isFinite(data)) {
    return data
  } else {
    let err = Error('Data Is Not Number')
    err.name = 'InvalidDataType'

    return err
  }
}
