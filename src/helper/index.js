const isArray = require('./isArray')
const isDate = require('./isDate')
const isEmail = require('./isEmail')
const isError = require('./isError')
const isNumber = require('./isNumber')
const isObject = require('./isObject')
const isString = require('./isString')

const checker = { isArray, isDate, isEmail, isError, isNumber, isObject, isString }

module.exports = { checker }
