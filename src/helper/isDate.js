module.exports = (data) => {
  if (!data) {
    return data
  } else if (data instanceof Date) {
    return data
  } else {
    let err = Error('Data Is Not Date')
    err.name = 'InvalidDataType'

    return err
  }
}
