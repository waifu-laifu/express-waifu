module.exports = (isType, isError, data) => {
  if (!data) {
    return data
  } else if (data && typeof data === 'object' && data.constructor === Array) {
    for (let index in data) {
      data[index] = isType(data[index])
    }

    for (let index in data) {
      if (isError(data[index])) {
        let err = Error(`Array Contain Invalid Data Type (${data[index].message})`)
        err.name = data[index].message || 'InvalidDataType'

        return err
      }
    }

    return data
  } else {
    let err = Error('Data Is Not Array')
    err.name = 'InvalidDataType'

    return err
  }
}
