module.exports = (data) => {
  return data instanceof Error && typeof data.message !== 'undefined'
}
