module.exports = (key, isType, isError, data, same = true) => {
  if (!data) {
    return data
  } else if (data && typeof data === 'object' && data.constructor === Object) {
    let dataKey = Object.keys(data)

    if (dataKey.length < key.length && same) {
      let err = Error(`Missing ${key.length - dataKey.length} Key In Object`)
      err.name = 'InvalidObjectKey'
      return err
    } else if (dataKey.length > key.length && same) {
      let err = Error(`Object Contain ${dataKey.length - key.length} More Key`)
      err.name = 'InvalidObjectKey'

      return err
    }

    for (let index in dataKey) {
      if (!(key.includes(dataKey[index]))) {
        let err = Error(`Object Contain Invalid Key (${dataKey[index]})`)
        err.name = 'InvalidObjectKey'

        return err
      }
    }

    for (let key in data) {
      data[key] = isType(data[key])
      if (isError(data[key])) {
        let err = Error(`Object Contain Invalid Data Type (${data[key].message})`)
        err.name = data[key].name || 'InvalidDataType'

        return err
      }
    }

    return data
  } else {
    let err = Error('Data Is Not Object !')
    err.name = 'InvalidDataType'

    return err
  }
}
