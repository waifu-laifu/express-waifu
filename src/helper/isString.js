module.exports = (data) => {
  if (!data) {
    return data
  } else if (typeof data === 'string' || data instanceof String) {
    return data
  } else {
    let err = Error('Data Is Not String')
    err.name = 'InvalidDataType'

    return err
  }
}
