const maria = require('./maria')
const mongo = require('./mongo')

module.exports = { maria, mongo }
