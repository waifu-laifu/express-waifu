const { MongoClient } = require('mongodb')
const { database: { mongo: config } } = require('../../config')

const mongo = async () => {
  try {
    const uri = `mongodb://${config.user}:${config.pass}@${config.host}:${config.port}/?authSource=admin`

    const client = new MongoClient(uri, { poolSize: 10, useNewUrlParser: true, useUnifiedTopology: true })
    await client.connect()

    const db = client.db(config.db)

    return db
  } catch (err) {
    console.log(err)
    throw err
  }
}

module.exports = mongo
