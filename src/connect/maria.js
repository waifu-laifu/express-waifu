const { createPool } = require('mariadb')
const { database: { maria: config } } = require('../../config')

const maria = async () => {
  try {
    const uri = `mariadb://${config.user}:${config.pass}@${config.host}:${config.port}/${config.db}?dateStrings=true`

    const pool = createPool(uri)
    const db = await pool.getConnection()

    return db
  } catch (err) {
    console.log(err)
    throw err
  }
}

module.exports = maria
