const express = require('express')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const router = require('./router')

const app = express()

app.enable('trust proxy')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(helmet())

app.use('/', router)

module.exports = app
