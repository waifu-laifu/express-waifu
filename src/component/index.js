const auth = require('./auth')
const user = require('./user')
const waifu = require('./waifu')

module.exports = { auth, user, waifu }
