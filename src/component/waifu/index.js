const waifu = require('./waifu')
const waifuBuild = require('./waifuBuild')
const waifuHandle = require('./waifuHandle')
const waifuQuery = require('./waifuQuery')
const waifuRoute = require('./waifuRoute')

module.exports = { waifu, waifuBuild, waifuHandle, waifuQuery, waifuRoute }
