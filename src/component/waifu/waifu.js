const Waifu = class Waifu {
  constructor (data) {
    this.fullname = data.fullname
    this.name = data.name
    this.alias = data.alias
    this.age = data.age
    this.birth = data.birth
    this.bwh = data.bwh
    this.blood = data.blood
    this.status = data.status
    this.pros = data.pros
    this.cons = data.cons
    this.bio = data.bio
    this.series = data.series
    this.added_by = data.added_by
    this.verified_by = data.verified_by
    this.liked_by = data.liked_by
    this.claimed_by = data.claimed_by
  }
}

module.exports = Waifu
