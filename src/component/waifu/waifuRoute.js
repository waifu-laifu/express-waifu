const { Router } = require('express')
const waifuHandle = require('./waifuHandle')
const { adaptRequest, sendResponse } = require('../../middleware')

const waifuRoute = Router()

waifuRoute.get('/', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await waifuHandle(request)

    if (data.length > 0) {
      sendResponse(res, 200, `${data.length} Data Found`, data)
    } else {
      sendResponse(res, 404, 'No Data Found', data)
    }
  } catch (err) {
    next(err)
  }
})

waifuRoute.get('/:id', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await waifuHandle(request)

    if (data.length > 0) {
      sendResponse(res, 200, `${data.length} Data Found`, data)
    } else {
      sendResponse(res, 404, 'No Data Found', data)
    }
  } catch (err) {
    next(err)
  }
})

waifuRoute.search('/', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await waifuHandle(request)

    if (data.length > 0) {
      sendResponse(res, 200, `${data.length} Data Found`, data)
    } else {
      sendResponse(res, 404, 'No Data Found', data)
    }
  } catch (err) {
    next(err)
  }
})

waifuRoute.post('/', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await waifuHandle(request)

    if (data.result.ok && data.insertedCount) {
      sendResponse(res, 201, 'A Data Has Been Inserted', data)
    } else {
      sendResponse(res, 400, 'Cannot Insert Data', data)
    }
  } catch (err) {
    next(err)
  }
})

waifuRoute.put('/:id', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await waifuHandle(request)

    if (data.result.ok && data.modifiedCount) {
      sendResponse(res, 201, 'A Data Has Been Modified', data)
    } else {
      sendResponse(res, 400, 'Cannot Modify Data', data)
    }
  } catch (err) {
    next(err)
  }
})

waifuRoute.patch('/:id', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await waifuHandle(request)

    if (data.result.ok && data.modifiedCount) {
      sendResponse(res, 200, 'A Data Has Been Modified', data)
    } else {
      sendResponse(res, 400, 'Cannot Modify Data', data)
    }
  } catch (err) {
    next(err)
  }
})

waifuRoute.delete('/:id', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await waifuHandle(request)

    if (data.result.ok && data.deletedCount) {
      sendResponse(res, 200, 'A Data Has Been Deleted', data)
    } else {
      sendResponse(res, 400, 'Cannot Delete Data', data)
    }
  } catch (err) {
    next(err)
  }
})

module.exports = waifuRoute
