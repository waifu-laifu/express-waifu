const Waifu = require('./waifu')

const waifuBuild = ({ isArray, isError, isNumber, isObject, isString }, data) => {
  const threeSize = ['bust', 'waist', 'hips']
  const nameObject = ['fname', 'mname', 'lname']

  const obj = {
    fullname: isString(data.fullname),
    name: isObject(nameObject, isString, isError, data.name, false),
    alias: isArray(isString, isError, data.alias),
    age: isNumber(data.age),
    birth: isString(data.birth),
    bwh: isObject(threeSize, isNumber, isError, data.bwh),
    blood: isString(data.blood),
    status: isString(data.status),
    pros: isArray(isString, isError, data.pros),
    cons: isArray(isString, isError, data.cons),
    bio: isString(data.bio),
    series: isString(data.series),
    added_by: isNumber(data.added_by),
    verified_by: isNumber(data.verified_by),
    liked_by: isArray(isNumber, isError, data.liked_by),
    claimed_by: isArray(isNumber, isError, data.claimed_by)
  }

  const waifus = new Waifu(obj)

  for (let key in waifus) {
    if (!waifus[key]) {
      delete waifus[key]
    }

    if (isError(waifus[key])) {
      let err = Error(`${key}: ${waifus[key].message}`)
      err.name = `BuildError: ${waifus[key].name}`

      return err
    }
  }

  return waifus
}

module.exports = waifuBuild
