const { checker } = require('../../helper')
const waifuBuild = require('./waifuBuild')
const waifuQuery = require('./waifuQuery')

const getWaifu = async () => {
  try {
    const data = await waifuQuery.getWaifu()
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const detailWaifu = async (id) => {
  try {
    const data = await waifuQuery.detailWaifu(id)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const searchWaifu = async (waifu) => {
  try {
    const data = await waifuQuery.searchWaifu(waifu)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const postWaifu = async (waifu) => {
  try {
    const data = await waifuQuery.addWaifu(waifu)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const putWaifu = async (waifu, id) => {
  try {
    const data = await waifuQuery.replaceWaifu(waifu, id)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const patchWaifu = async (waifu, id) => {
  try {
    const data = await waifuQuery.updateWaifu(waifu, id)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const deleteWaifu = async (id) => {
  try {
    const data = await waifuQuery.deleteWaifu(id)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const waifuHandle = (request) => {
  const { body, method, params } = request

  const waifu = waifuBuild(checker, body)
  const id = params.id

  if (waifu instanceof Error) {
    console.log(waifu)
    throw waifu
  }

  switch (method) {
    case 'GET':
      if (id) {
        return detailWaifu(id)
      } else {
        return getWaifu()
      }
    case 'SEARCH':
      return searchWaifu(waifu)
    case 'POST':
      return postWaifu(waifu)
    case 'PUT':
      return putWaifu(waifu, id)
    case 'PATCH':
      return patchWaifu(waifu, id)
    case 'DELETE':
      return deleteWaifu(id)
    default:
      break
  }
}

module.exports = waifuHandle
