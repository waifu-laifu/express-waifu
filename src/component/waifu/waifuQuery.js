const { ObjectId } = require('mongodb')
const { mongo } = require('../../connect')

const waifuQuery = () => {
  const db = mongo()

  return Object.freeze({
    getWaifu,
    detailWaifu,
    searchWaifu,
    addWaifu,
    replaceWaifu,
    updateWaifu,
    deleteWaifu
  })

  async function getWaifu () {
    const waifus = await db
    const Waifu = waifus.collection('waifus')

    try {
      const res = await Waifu.find().toArray()
      return res
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  async function detailWaifu (id) {
    const waifus = await db
    const Waifu = waifus.collection('waifus')

    try {
      const res = await Waifu.find({ _id: new ObjectId(id) }).toArray()
      return res
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  async function searchWaifu (waifu) {
    const waifus = await db
    const Waifu = waifus.collection('waifus')

    try {
      const res = await Waifu.find(waifu).toArray()
      return res
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  async function addWaifu (waifu) {
    const waifus = await db
    const Waifu = waifus.collection('waifus')

    try {
      const res = await Waifu.insertOne(waifu)
      return res
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  async function replaceWaifu (waifu, id) {
    const waifus = await db
    const Waifu = waifus.collection('waifus')

    try {
      const res = await Waifu.replaceOne({ _id: new ObjectId(id) }, waifu)
      return res
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  async function updateWaifu (waifu, id) {
    const waifus = await db
    const Waifu = waifus.collection('waifus')

    try {
      const res = await Waifu.updateOne({ _id: new ObjectId(id) }, { $set: waifu })
      return res
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  async function deleteWaifu (id) {
    const waifus = await db
    const Waifu = waifus.collection('waifus')

    try {
      const res = await Waifu.deleteOne({ _id: new ObjectId(id) })
      return res
    } catch (err) {
      console.log(err)
      throw err
    }
  }
}

module.exports = waifuQuery()
