const { maria } = require('../../connect')

const userQuery = () => {
  const db = maria()

  return Object.freeze({
    getUser,
    detailUser,
    searchUser,
    addUser,
    replaceUser,
    updateUser,
    deleteUser
  })

  async function getUser () {
    const users = await db
    const sql = 'SELECT * FROM user'

    try {
      const res = await users.query(sql)
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      users.release()
    }
  }

  async function detailUser (id) {
    const users = await db
    const sql = 'SELECT * FROM user WHERE user_id = ?'

    try {
      const res = await users.query(sql, [id])
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      users.release()
    }
  }

  async function searchUser (keyword) {
    const users = await db
    const value = [`%${keyword}%`, `%${keyword}%`, `%${keyword}%`]
    const sql = `
          SELECT * FROM user
          WHERE
          user_name LIKE ? OR
          user_fullname LIKE ? OR
          user_email LIKE ?
    `

    try {
      const res = await users.query(sql, value)
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      users.release()
    }
  }

  async function addUser (user) {
    const users = await db
    const value = Object.values(user)
    const sql = `
          INSERT INTO user
          SET
          user_name = ?,
          user_fullname = ?,
          user_email = ?,
          user_password = ?,
          user_about = ?,
          user_joined = NOW()
    `

    try {
      const res = await users.query(sql, value)
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      users.release()
    }
  }

  async function replaceUser (user, id) {
    const users = await db
    const value = Object.values(user)
    value.unshift(id)
    const sql = `
          REPLACE INTO user
          SET
          user_id = ?,
          user_name = ?,
          user_fullname = ?,
          user_email = ?,
          user_password = ?,
          user_about = ?,
          user_edited = NOW()
    `

    try {
      const res = await users.query(sql, value)
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      users.release()
    }
  }

  async function updateUser (user, id) {
    const users = await db
    const userKey = Object.keys(user)
    let data = ''

    for (let i = 0; i < userKey.length; i++) {
      data += `${userKey[i]} = '${user[userKey[i]]}',`
    }

    const sql = `
          UPDATE user
          SET
          ${data}
          user_edited = NOW()
          WHERE
          user_id = ?
    `

    try {
      const res = await users.query(sql, [id])
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      users.release()
    }
  }

  async function deleteUser (id) {
    const users = await db
    const sql = `
          DELETE FROM user
          WHERE
          user_id = ?
    `

    try {
      const res = await users.query(sql, [id])
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      users.release()
    }
  }
}

module.exports = userQuery()
