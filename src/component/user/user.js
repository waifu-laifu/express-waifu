const user = class User {
  constructor (data) {
    this.user_name = data.user_name
    this.user_fullname = data.user_fullname
    this.user_email = data.user_email
    this.user_password = data.user_password
    this.user_about = data.user_about
  }
}

module.exports = user
