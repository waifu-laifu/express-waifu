const User = require('./user')

const userBuild = async ({ isError, isString }, data, bcrypt) => {
  const obj = {
    user_name: isString(data.user_name),
    user_fullname: isString(data.user_fullname),
    user_email: isString(data.user_email),
    user_password: isString(data.user_password),
    user_about: isString(data.user_about)
  }

  if (Object.keys(data).includes('user_password')) {
    try {
      obj.user_password = await bcrypt.hash(obj.user_password, 12)
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const users = new User(obj)

  for (let key in users) {
    if (!users[key]) {
      delete users[key]
    }

    if (isError(users[key])) {
      let err = Error(`${key}: ${users[key].message}`)
      err.name = `BuildError: ${users[key].name}`

      return err
    }
  }

  return users
}

module.exports = userBuild
