const { Router } = require('express')
const userHandle = require('./userHandle')
const { adaptRequest, sendResponse } = require('../../middleware')

const userRoute = Router()

userRoute.get('/', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await userHandle(request)

    if (data.length > 0) {
      sendResponse(res, 200, `${data.length} Data Found`, data)
    } else {
      sendResponse(res, 404, 'No Data Found', data)
    }
  } catch (err) {
    next(err)
  }
})

userRoute.get('/:id', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await userHandle(request)

    if (data.length > 0) {
      sendResponse(res, 200, `${data.length} Data Found`, data)
    } else {
      sendResponse(res, 404, 'No Data Found', data)
    }
  } catch (err) {
    next(err)
  }
})

userRoute.search('/', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await userHandle(request)

    if (data.length > 0) {
      sendResponse(res, 200, `${data.length} Data Found`, data)
    } else {
      sendResponse(res, 404, 'No Data Found', data)
    }
  } catch (err) {
    next(err)
  }
})

userRoute.post('/', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await userHandle(request)

    if (data.insertId && data.insertId > 0) {
      sendResponse(res, 201, 'A Data Has Been Inserted', data)
    } else {
      sendResponse(res, 400, 'Cannot Insert Data', data)
    }
  } catch (err) {
    next(err)
  }
})

userRoute.put('/:id', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await userHandle(request)

    if (data.affectedRows && data.affectedRows > 0) {
      sendResponse(res, 201, 'A Data Has Been Modified', data)
    } else {
      sendResponse(res, 400, 'Cannot Modify Data', data)
    }
  } catch (err) {
    next(err)
  }
})

userRoute.patch('/:id', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await userHandle(request)

    if (data.affectedRows && data.affectedRows > 0) {
      sendResponse(res, 200, 'A Data Has Been Modified', data)
    } else {
      sendResponse(res, 400, 'Cannot Modify Data', data)
    }
  } catch (err) {
    next(err)
  }
})

userRoute.delete('/:id', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await userHandle(request)

    if (data.affectedRows && data.affectedRows > 0) {
      sendResponse(res, 200, 'A Data Has Been Deleted', data)
    } else {
      sendResponse(res, 400, 'Cannot Delete Data', data)
    }
  } catch (err) {
    next(err)
  }
})

module.exports = userRoute
