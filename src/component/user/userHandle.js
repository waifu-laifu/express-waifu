const bcrypt = require('bcrypt')
const { checker } = require('../../helper')
const userBuild = require('./userBuild')
const userQuery = require('./userQuery')

const getUser = async () => {
  try {
    const data = await userQuery.getUser()
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const detailUser = async (id) => {
  try {
    const data = await userQuery.detailUser(id)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const searchUser = async (keyword) => {
  try {
    const data = await userQuery.searchUser(keyword)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const postUser = async (user) => {
  try {
    const data = await userQuery.addUser(user)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const putUser = async (user, id) => {
  try {
    const data = await userQuery.replaceUser(user, id)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const patchUser = async (user, id) => {
  try {
    const data = await userQuery.updateUser(user, id)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const deleteUser = async (id) => {
  try {
    const data = await userQuery.deleteUser(id)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const userHandle = async (request) => {
  const { body, method, params, query } = request

  const user = await userBuild(checker, body, bcrypt)
  const keyword = query.keyword
  const id = params.id

  if (user instanceof Error) {
    console.log(user)
    throw user
  }

  switch (method) {
    case 'GET':
      if (id) {
        return detailUser(id)
      } else {
        return getUser()
      }
    case 'SEARCH':
      return searchUser(keyword)
    case 'POST':
      return postUser(user)
    case 'PUT':
      return putUser(user, id)
    case 'PATCH':
      return patchUser(user, id)
    case 'DELETE':
      return deleteUser(id)
    default:
      break
  }
}

module.exports = userHandle
