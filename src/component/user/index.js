const user = require('./user')
const userBuild = require('./userBuild')
const userHandle = require('./userHandle')
const userQuery = require('./userQuery')
const userRoute = require('./userRoute')

module.exports = { user, userBuild, userHandle, userQuery, userRoute }
