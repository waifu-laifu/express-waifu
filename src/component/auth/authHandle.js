const bcrypt = require('bcrypt')
const { checker } = require('../../helper')
const authBuild = require('./authBuild')
const authQuery = require('./authQuery')

const comparePassword = async (password, hash) => {
  try {
    const res = bcrypt.compare(password, hash)

    if (res) {
      return res
    } else {
      let error = Error('Invalid Password !')
      error.status = 400
      error.name = 'BadRequest'

      throw error
    }
  } catch (err) {
    console.log(err)
    throw err
  }
}

const encryptPassword = async (password, salt) => {
  try {
    const res = await bcrypt.hash(password, salt)
    return res
  } catch (err) {
    console.log(err)
    throw err
  }
}

const authProfile = async (authData) => {
  try {
    const data = await authQuery.getProfile(authData)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const authLogin = async (password, authData) => {
  try {
    const data = await authQuery.authLogin(authData)
    const res = await comparePassword(password, data[0].user_password)

    if (res) return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const authRegister = async (authData) => {
  try {
    const data = await authQuery.authRegister(authData)
    return data
  } catch (err) {
    console.log(err)
    throw err
  }
}

const authHandle = async (request) => {
  const { body, method, path } = request

  let authData = await authBuild(checker, body)

  if (authData instanceof Error) {
    console.log(authData)
    throw authData
  }

  try {
    authData.user_password = await encryptPassword(authData.user_password, 12)
  } catch (err) {
    console.log(err)
    throw err
  }

  switch (method) {
    case 'GET':
      return authProfile(authData)
    case 'POST':
      if (path === '/login') {
        return authLogin(body.user_password, authData)
      } else {
        return authRegister(authData)
      }
    default: {
      let err = Error('Method Not Found')
      err.status = 404
      err.name = 'MethodError'

      throw err
    }
  }
}

module.exports = authHandle
