const { maria } = require('../../connect')

const authQuery = () => {
  const db = maria()

  return Object.freeze({
    getProfile,
    authLogin,
    authRegister
  })

  async function getProfile (authData) {
    const auth = await db
    const value = [authData.user_id, authData.user_id, authData.user_password]
    const sql = `SELECT
                  user_id,
                  user_name,
                  user_fullname,
                  user_email,
                  user_about,
                  user_roles
                FROM user
                WHERE (user_name = ? OR user_email = ?)
                AND user_password = ?
                `

    try {
      const res = await auth.query(sql, value)
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      auth.release()
    }
  }

  async function authLogin (authData) {
    const auth = await db
    const value = [
      authData.user_name || authData.user_email,
      authData.user_name || authData.user_email
    ]
    const sql = 'SELECT user_id, user_fullname, user_roles, user_password FROM user WHERE user_name = ? OR user_email = ?'

    try {
      const res = await auth.query(sql, value)
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      auth.release()
    }
  }

  async function authRegister (authData) {
    const auth = await db
    const value = Object.values(authData)
    const sql = `
      INSERT INTO user
      SET
      user_name = ?,
      user_email = ?,
      user_password = ?
    `

    try {
      const res = await auth.query(sql, value)
      return res
    } catch (err) {
      console.log(err)
      throw err
    } finally {
      auth.release()
    }
  }
}

module.exports = authQuery()
