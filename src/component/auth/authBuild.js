const Auth = require('./auth')

const obj = (data, isString, isEmail) => {
  if (Object.keys(data).length > 2) {
    return {
      user_name: isString(data.user_name),
      user_email: isString(data.user_email),
      user_password: isString(data.user_password)
    }
  } else {
    if (isEmail(data.user_id)) {
      return {
        user_email: isString(data.user_id),
        user_password: isString(data.user_password)
      }
    } else {
      return {
        user_name: isString(data.user_id),
        user_password: isString(data.user_password)
      }
    }
  }
}

const authBuild = async ({ isEmail, isError, isString }, data) => {
  const object = obj(data, isString, isEmail)
  const authData = new Auth(object)

  for (let key in authData) {
    if (!authData[key]) {
      delete authData[key]
    }

    if (isError(authData[key])) {
      let err = Error(`${key}: ${authData[key].message}`)
      err.name = `BuildError: ${authData[key].name}`

      return err
    }
  }

  return authData
}

module.exports = authBuild
