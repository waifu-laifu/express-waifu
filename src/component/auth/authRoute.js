const { Router } = require('express')
const authHandle = require('./authHandle')
const { adaptRequest, signToken, sendResponse } = require('../../middleware')

const authRoute = Router()

authRoute.post('/login', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    let data = await authHandle(request)

    if (data.length > 0) {
      data = signToken(data[0])
      sendResponse(res, 200, 'Login Success', data)
    } else {
      sendResponse(res, 404, 'No Data Found', data)
    }
  } catch (err) {
    next(err)
  }
})

authRoute.post('/register', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    let data = await authHandle(request)

    if (data.length > 0) {
      sendResponse(res, 200, 'Register Success', data)
    } else {
      sendResponse(res, 404, 'No Data Found', data)
    }
  } catch (err) {
    next(err)
  }
})

authRoute.get('/profile', async (req, res, next) => {
  const request = adaptRequest(req)

  try {
    const data = await authHandle(request)

    if (data.length > 0) {
      sendResponse(res, 200, `${data.length} Data Found`, data)
    } else {
      sendResponse(res, 404, 'No Data Found', data)
    }
  } catch (err) {
    next(err)
  }
})

module.exports = authRoute
