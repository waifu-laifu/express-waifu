const auth = require('./auth')
const authBuild = require('./authBuild')
const authHandle = require('./authHandle')
const authQuery = require('./authQuery')
const authRoute = require('./authRoute')

module.exports = { auth, authBuild, authHandle, authQuery, authRoute }
