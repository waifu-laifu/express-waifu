const { Router } = require('express')
const { authorizeUser, checkMethod, requireData, handleError } = require('./middleware')
const { waifu: { waifuRoute } } = require('./component')
const { user: { userRoute } } = require('./component')
const { auth: { authRoute } } = require('./component')

const router = Router()

router.use([authorizeUser, checkMethod, requireData])

router.get('/', (req, res) => {
  res.send('Express.js API running')
})

router.use('/waifu', waifuRoute)
router.use('/user', userRoute)
router.use('/auth', authRoute)

router.use((req, res, next) => {
  return res.status(404).json({
    status: 404,
    message: `Cannot find '${req.url}' in this server`
  })
})

router.use(handleError)

module.exports = router
