DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `admin_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `admin_user_id` int(12) unsigned NOT NULL,
  `admin_joined` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `admin_user_id` (`admin_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `role_id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(30) DEFAULT NULL,
  `role_type` int(2) unsigned NOT NULL,
  `role_code` varchar(20) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `unique_code` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

LOCK TABLES `roles` WRITE;
INSERT INTO `roles` VALUES (1,'Root Admin',0,'ROOT'),(2,'Developer Admin',1,'DEV'),(3,'Forum Moderator',1,'MODERATOR'),(4,'Validator',1,'VALIDATE'),(5,'Admin',1,'ADMIN'),(6,'Viscount',2,'VISCOUNT'),(7,'Baron',2,'BARON'),(8,'Waifu Evangelist',2,'EVANGELIST'),(9,'Waifu Connoisseur',2,'CONNOISSEUR'),(10,'Regular User',3,'REGULAR');
UNLOCK TABLES;

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL,
  `user_fullname` varchar(50) DEFAULT NULL,
  `user_email` varchar(320) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_about` varchar(500) DEFAULT NULL,
  `user_joined` datetime DEFAULT current_timestamp(),
  `user_edited` datetime DEFAULT NULL,
  `user_roles` int(2) NOT NULL DEFAULT 9,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `unique_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger check_user_insert after insert on user for each row begin if new.user_roles < 6 then replace into admin set admin_user_id = new.user_id, admin_joined = new.user_joined; end if; end */;;
DELIMITER ;

DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger check_user_update after update on user for each row begin if new.user_roles < 6 then replace into admin set admin_user_id = new.user_id, admin_joined = now(); end if; end */;;
DELIMITER ;

DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger check_user_delete after delete on user for each row begin if old.user_roles < 6 then delete from admin where admin_user_id = old.user_id; end if; end */;;
DELIMITER ;
