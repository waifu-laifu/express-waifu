# Configuration Guide

## Prerequisities
* OpenSSL (Linux)

## Generate Self-Signed Certificate and Key
**1. Generate Encrypted Keys and Certificate**

`openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem`

**2. Save Passphrase in config.json**

```
"cCreate" : {
  "server": "your_passphrase_here"
}
```

## Generate RSA Private and Public Key
**1. Generate Encrypted Private RSA Keys**

`openssl genrsa -des3 -out pri.pem 2048`

**2. Export Public RSA Keys**

`openssl rsa -in pri.pem -outform PEM -pubout -out pub.pem`

**3. Save Passphrase in config.json**

```
"cCreate" : {
  "token": "your_passphrase_here"
}
```