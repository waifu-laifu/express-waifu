const { readFileSync } = require('fs')

module.exports = JSON.parse(readFileSync('./config/config.json'))
