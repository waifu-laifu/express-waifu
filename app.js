const { createServer } = require('https')
const { readFileSync } = require('fs')
const { port, host, cCreate } = require('./config')
const app = require('./src')

const certificate = readFileSync('./config/certificate.pem')
const key = readFileSync('./config/key.pem')
const server = createServer({ cert: certificate, key: key, passphrase: cCreate.server }, app)

server.listen(port, host, () => {
  console.log(`Express.js API Running On Port: ${port}`)
})
