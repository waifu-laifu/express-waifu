# Backend for Waifu Rest API

>  Waifu for Laifu !

## Structure
```
  app.js
  |-- config                              # Configuration files
  |-- dump                                # Database dump
  |-- src
  |   |-- index.js                        # Server declaration
  |   |-- router.js                       # Server route
  |   |-- component                       # Application services
  |   |   |-- services1
  |   |   |   |-- service.js              # Object model
  |   |   |   |-- serviceBuild.js         # Spawn object from model
  |   |   |   |-- serviceHandle.js        # Handling request from user
  |   |   |   |-- serviceQuery.js         # Database query
  |   |   |   |-- serviceRoute.js         # Service request entrypoint
  |   |   |-- ...
  |   |   |-- servicesN
  |   |   |   |-- ...
  |   |-- connect                         # Database connection
  |   |-- helper                          # Helper function
  |   |-- middleware                      # I/O controller
  |-- test                                # Tests suite
```

## Features
 * Clean architecture
 * Dependency injection
 * Error handling
 * DB pooling
 * Test suite
 * Basic Authentication

## Prerequisities
 * Node.js
 * MongoDB
 * MariaDB

## Usage
**1. Clone and install**
```
git clone
cd express-backend
npm i
```
**2. Setting config file**

Rename `example.config.json` into `config.json`, then add your database and jwt info

Add your ssl `cert.pem` and `key.pem` into config folder

Add your rsa `pri.pem` and `pub.pem` into config folder

> More info check [config](../blob/master/config/README.md)

**3. Start database**

`mongod`

**4. Start server**

Start development server, with auto-restart using

`npm run dev`

Or Start production server, using

`npm run start`

> Feel free to correct